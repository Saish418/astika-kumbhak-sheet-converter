package com.sheetconvetor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AstikaKumbhakSheetConverterApplication {

	public static void main(String[] args) {
		SpringApplication.run(AstikaKumbhakSheetConverterApplication.class, args);
	}

}
