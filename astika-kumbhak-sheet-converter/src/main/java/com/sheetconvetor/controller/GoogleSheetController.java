package com.sheetconvetor.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.sheetconvetor.dao.ClientMasterDao;
import com.sheetconvetor.dao.ParseCountDao;
import com.sheetconvetor.google.GoogleSheetReader;
import com.sheetconvetor.model.ClientMaster;
import com.sheetconvetor.model.SheetParseCount;
import com.sheetconvetor.util.HighestAndLowest;
import com.sheetconvetor.util.HtmlToWord;

@Controller
@RequestMapping("/astika-kumbhak")
public class GoogleSheetController {

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private GoogleSheetReader googleSheetReader;

	@Autowired
	private ClientMasterDao clientMasterDao;

	@Autowired
	private ParseCountDao parseCountDao;

	@Autowired
	private HttpSession session;

	/* purpose - index page mapping */
	@GetMapping("/index")
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("index");
		return modelAndView;
	}

	@GetMapping("/reload-excelsheet")
	public ModelAndView reloadExcelsheet() {
		ModelAndView modelAndView = new ModelAndView("index");
		Map<String, Object> model = new HashMap<String, Object>();
		try {

			Optional<SheetParseCount> sheetParseCount = parseCountDao.findById(1);

			int count = 1;
			SheetParseCount s = null;
			if (!sheetParseCount.isEmpty()) {
				s = sheetParseCount.get();

				if (s != null) {
					count = s.getCount();
				}
			} else {
				count = count + 1;
			}
			List<ClientMaster> list = googleSheetReader.readGoogleSheet(count);

			List<ClientMaster> savedList = clientMasterDao.saveAll(list);

			SheetParseCount newObj = new SheetParseCount();
			if (s != null) {
				newObj.setId(s.getId());
				newObj.setCount((s.getCount()) + (list.size() - 1));
			} else {
				newObj.setCount(count + list.size());
			}

			parseCountDao.save(newObj);

			String path = generateDocs(savedList);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		modelAndView.addObject("list", clientMasterDao.findAll());
		return modelAndView;
	}

	public String generateDocs(List<ClientMaster> list) {

		String path = "";
		try {

			for (ClientMaster clientMaster : list) {

				// B col
				char genChar = clientMaster.getGender().charAt(0);
				String bCol = genChar + "/" + clientMaster.getAge() + "/" + clientMaster.getHeight() + "/"
						+ clientMaster.getWeight();

				// D Col

				String problemsArray[] = clientMaster.getProblemsType().split(",");

				List<String> DEList=new ArrayList<String>();
				
				String DE = "";
				for (int i = 0; i < problemsArray.length; i++) {

					if (problemsArray[i]
							.equalsIgnoreCase("Usually I face problems in the lower part of my body (below hips).")) {
						DEList.add("D");
					}
					if (problemsArray[i].equalsIgnoreCase(" Usually the problem happens between hips and chest.")) {
						DEList.add("M");
					}
					if (problemsArray[i].equalsIgnoreCase(" I get frequent problems above the chest area.")) {
						DEList.add("U");
					}

				}
				DE = StringUtils.join(DEList, ',');
				//DE = DE.replaceAll(" ", ",");

				// E Col

				String E = "";
				if (clientMaster.getBodySideProblem()
						.equalsIgnoreCase("Left side of my body is relatively weak and has problem.")) {

					E = "L";

				} else if (clientMaster.getBodySideProblem()
						.equalsIgnoreCase("Right side of my body is weak and usually face problems.")) {

					E = "R";
				} else if (clientMaster.getBodySideProblem().equalsIgnoreCase("Both sides have problems.")) {
					E = "B";
				} else {
					E = "N/A";
				}

				DE = DE + " / " + E;

				String FHGI = clientMaster.getInhaleCount() + " : " + clientMaster.getInhalation_hold_count() + " : "
						+ clientMaster.getExhalationLength() + " : " + clientMaster.getExhalationHoldCount();

				// JKLMN

				int arrayJKLMN[] = new int[] { Integer.parseInt(clientMaster.getExhautingExercise()),
						Integer.parseInt(clientMaster.getCry()), Integer.parseInt(clientMaster.getHungerAndDigestion()),
						Integer.parseInt(clientMaster.getWalkingAndRunning()),
						Integer.parseInt(clientMaster.getSmoothLife()) };

				List<Integer> lowest = HighestAndLowest.lowest(arrayJKLMN);
				List<Integer> height = HighestAndLowest.height(arrayJKLMN);
				List<String> finalHeightList = new ArrayList<String>();
				List<String> finalLowestList = new ArrayList<String>();

				for (Integer i : lowest) {

					if (i == 0)
						finalLowestList.add("P");

					else if (i == 1)
						finalLowestList.add("J");

					else if (i == 2)
						finalLowestList.add("AG");

					else if (i == 3)
						finalLowestList.add("V");

					else if (i == 4)
						finalLowestList.add("AK");

				}

				for (Integer i : height) {

					if (i == 0)
						finalHeightList.add("P");

					else if (i == 1)
						finalHeightList.add("J");

					else if (i == 2)
						finalHeightList.add("AG");

					else if (i == 3)
						finalHeightList.add("V");

					else if (i == 4)
						finalHeightList.add("AK");

				}

				String low = StringUtils.join(finalLowestList, ',');
				String high = StringUtils.join(finalHeightList, ',');

				String JKLMN = low + " -U / " + high + " -O";

				String P = "";

				if (clientMaster.getBodyDominant().equalsIgnoreCase("More aches and pains, thin body, nerve issue")) {
					P = "VATA";
				} else if (clientMaster.getBodyDominant()
						.equalsIgnoreCase("More acidity, burning sensation, cold issues, skin issues, pigmentation.")) {
					P = "PITTA";
				} else if (clientMaster.getBodyDominant()
						.equalsIgnoreCase("Weight issue, lethargy, procrastination, cough, sluggish digestion.")) {
					P = "KAPHA";
				} else {
					P = "None";
				}

				String T = clientMaster.getMedicineAndQty();

				if (T.equals("")) {
					T = "None";
				}

				request.setAttribute("id", clientMaster.getClientId());
				request.setAttribute("a", clientMaster.getName());
				request.setAttribute("b", bCol);
				request.setAttribute("c", clientMaster.getProgramEnrolled());
				request.setAttribute("de", DE);
				request.setAttribute("s", clientMaster.getOtherConditions());
				request.setAttribute("t", T);
				request.setAttribute("r", clientMaster.getPregnant());
				request.setAttribute("fghi", FHGI);
				request.setAttribute("jklmn", JKLMN);
				request.setAttribute("p", P);
				request.setAttribute("o", clientMaster.getEating());
				request.setAttribute("q", clientMaster.getPhoto().replaceAll("open", "uc"));

//				String jspUrl = "http://localhost:8080/astika-kumbhak/loadPatientReport/" + clientMaster.getName() + "/"
//						+ bCol + "/" + clientMaster.getProgramEnrolled() + "/" + DE + "/"
//						+ clientMaster.getOtherConditions() + "/" + T + "/"
//						+ clientMaster.getPregnant() + "/" + FHGI + "/" + JKLMN + "/" + P + "/"
//						+ clientMaster.getEating() + "/" + clientMaster.getPhoto();

				String jspUrl = "http://localhost:8080/astika-kumbhak/loadPatientReport";
				// String htmlString = HtmlToString.convert(jspUrl);
				path = HtmlToWord.generate(jspUrl, request);
				clientMasterDao.updatePath(path, clientMaster.getClientId());

				// html2pdf(jspUrl,".pdf","");

			}
			return path;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

//	@GetMapping("/loadPatientReport/{a}/{b}/{c}/{de}/{s}/{t}/{r}/{fghi}/{jklmn}/{p}/{o}/{q}")
//	@ResponseBody
//	public ModelAndView loadDealerCertificate(@PathVariable(value = "a") String a, @PathVariable(value = "b") String b,
//			@PathVariable(value = "c") String c, @PathVariable(value = "de") String de,
//			@PathVariable(value = "s") String s, @PathVariable(value = "t") String t,
//			@PathVariable(value = "t") String r, @PathVariable(value = "fghi") String fghi,
//			@PathVariable(value = "jklmn") String jklmn, @PathVariable(value = "p") String p,
//			@PathVariable(value = "o") String o, @PathVariable(value = "q") String q) {

	@GetMapping("/loadPatientReport")
	@ResponseBody
	public ModelAndView loadDealerCertificate() {
		try {
			Map<String, Object> model = new HashMap<String, Object>();

			System.out.println(request.getAttribute("a"));
			model.put("a", request.getAttribute("a"));
			model.put("b", request.getAttribute("b"));
			model.put("c", request.getAttribute("c"));
			model.put("de", request.getAttribute("de"));
			model.put("s", request.getAttribute("s"));
			model.put("t", request.getAttribute("t"));
			model.put("r", request.getAttribute("r"));
			model.put("fghi", request.getAttribute("fghi"));
			model.put("jklmn", request.getAttribute("jklmn"));
			model.put("p", request.getAttribute("p"));
			model.put("o", request.getAttribute("o"));
			model.put("q", request.getAttribute("q"));

			return new ModelAndView("word-template", model);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}