package com.sheetconvetor.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sheetconvetor.model.ClientMaster;

@Repository
public interface ClientMasterDao extends JpaRepository<ClientMaster, Integer> {

	@org.springframework.transaction.annotation.Transactional
	@Modifying
	@Query("UPDATE ClientMaster SET document =:path WHERE clientId=:id")
	void updatePath(@Param(value="path") String path,@Param(value="id")Integer id);

}
