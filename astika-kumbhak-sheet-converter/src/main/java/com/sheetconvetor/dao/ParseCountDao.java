package com.sheetconvetor.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sheetconvetor.model.SheetParseCount;

@Repository
public interface ParseCountDao extends JpaRepository<SheetParseCount, Integer> {

}
