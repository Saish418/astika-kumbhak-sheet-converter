package com.sheetconvetor.google;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.sheetconvetor.dao.ParseCountDao;
import com.sheetconvetor.model.ClientMaster;

@Component
public class GoogleSheetReader {

	@Autowired
	private HttpServletRequest request;

	private static final String APPLICATION_NAME = "Google Sheets API Java Quickstart";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";

	/**
	 * Global instance of the scopes required by this quickstart. If modifying these
	 * scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY);
	private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

	/**
	 * Prints the names and majors of students in a sample spreadsheet:
	 * https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
	 * 
	 * @throws IOException
	 * @throws GeneralSecurityException
	 * @throws InterruptedException
	 */

	public List<ClientMaster> readGoogleSheet(int count)
			throws GeneralSecurityException, IOException, InterruptedException {

		List<ClientMaster> clientMastersList = new ArrayList<ClientMaster>();

		try {

			// Build a new authorized API client service.
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			final String[] spreadsheetId = { "1T1HiKiLCBA_eZS9d88mnn25b87w3JoysqOHTnOW_nWU" };
			final String[] range = { "Form Responses 1!A" + count + ":Z" };

			for (int i = 0; i < spreadsheetId.length; i++) {

				for (int j = i; j <= i; j++) {

					Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
							.setApplicationName(APPLICATION_NAME).build();
					ValueRange response = service.spreadsheets().values().get(spreadsheetId[i], range[j]).execute();
					List<List<Object>> values = response.getValues();
					if (values == null || values.isEmpty()) {
						System.out.println("No data found.");
					} else {
						// System.out.println(values.size());
						if (i == 0) {
							System.out.println("Start");

							ClientMaster clientMaster = null;
							for (List row : values) {

								clientMaster = new ClientMaster();
								// System.out.println(row.get(0));
								clientMaster.setEmail(String.valueOf(row.get(1)));
								clientMaster.setMobile(String.valueOf(row.get(3)));
								clientMaster.setName(String.valueOf(row.get(2)));
								clientMaster.setAge(Integer.parseInt(String.valueOf(row.get(4))));
								clientMaster.setGender(String.valueOf(row.get(5)));
								clientMaster.setHeight(Float.valueOf(String.valueOf(row.get(6))));
								clientMaster.setWeight(Float.valueOf(String.valueOf(row.get(7))));
								clientMaster.setProgramEnrolled(String.valueOf(row.get(8)));
								clientMaster.setProblemsType(String.valueOf(row.get(9)));
								clientMaster.setBodySideProblem(String.valueOf(row.get(10)));
								clientMaster.setInhaleCount(String.valueOf(row.get(11)));
								clientMaster.setExhalationLength(String.valueOf(row.get(12)));
								clientMaster.setInhalation_hold_count(String.valueOf(row.get(13)));
								clientMaster.setExhalationHoldCount(String.valueOf(row.get(14)));
								clientMaster.setExhautingExercise(String.valueOf(row.get(15)));
								clientMaster.setCry(String.valueOf(row.get(16)));
								clientMaster.setHungerAndDigestion(String.valueOf(row.get(17)));
								clientMaster.setWalkingAndRunning(String.valueOf(row.get(18)));
								clientMaster.setSmoothLife(String.valueOf(row.get(19)));
								clientMaster.setEating(String.valueOf(row.get(20)));
								clientMaster.setBodyDominant(String.valueOf(row.get(21)));
								clientMaster.setPhoto(String.valueOf(row.get(22)));
								clientMaster.setPregnant(String.valueOf(row.get(23)));
								clientMaster.setMedicineAndQty(String.valueOf(row.get(24)));
								clientMaster.setOtherConditions(String.valueOf(row.get(25)));
								clientMastersList.add(clientMaster);
							}

						}

					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			return clientMastersList;
		}
		return clientMastersList;
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException If the credentials.json file cannot be found.
	 */
	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		// Load client secrets.
		InputStream in = GoogleSheetReader.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES)
						.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
						.setAccessType("offline").build();
		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}

}
