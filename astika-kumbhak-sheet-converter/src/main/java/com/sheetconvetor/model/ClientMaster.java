package com.sheetconvetor.model;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "client_master")
@Data
public class ClientMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="client_id")
	private Integer clientId;
	
	private String email;
	
	private String name;
	
	private String mobile;
	
	private Integer age;
	
	private String gender;
	
	private Float height;
	
	// in kilograms
	private Float weight;
	
	@Column(name="program_enrolled")
	private String programEnrolled;
	
	@Column(name="problems_type")
    private String problemsType;

	@Column(name="body_side_problem")
	private String bodySideProblem; 
	
	@Column(name="inhale_count")
	private String inhaleCount; 
	
	@Column(name="exhalation_length")
	private String exhalationLength; 
	
	@Column(name="exhalation_hold_count")
	private String exhalationHoldCount; 
	
	@Column(name="inhalation_hold_count")
	private String inhalation_hold_count; 
	
	//j
	@Column(name="exhauting_exercise")
	private String exhautingExercise; 
	
	//k
	@Column(name="cry")
	private String cry; 
	
	//l
	@Column(name="hunger_and_digestion")
	private String hungerAndDigestion; 
		
	//m
	@Column(name="walking_and_running")
	private String walkingAndRunning; 
	
	//n
	@Column(name="smooth_life")
	private String smoothLife; 
	
	@Column(name="eating")
	private String eating;
	
	@Column(name="body_dominant")
	private String bodyDominant; 
	
	@Column(name="photo")
	private String photo; 
	
	@Column(name="medicine_and_qty")
	private String medicineAndQty; 
	
	@Column(name="pregnant")
	private String pregnant; 
	
	@Column(name="other_conditions")
	private String otherConditions; 
	
	@Column(name="document")
	private String document; 
	
	 
	

	
	
}
