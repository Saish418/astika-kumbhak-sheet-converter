package com.sheetconvetor.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HighestAndLowest {

	public static List<Integer> height(int arrayJKLMN[]) {
		int large;
		List<Integer> largestList = new ArrayList<>();

		Arrays.sort(arrayJKLMN);

		int neArr[] = new int[5];
		for (int i = 0; i < arrayJKLMN.length; i++) {
			neArr[i] = arrayJKLMN[i];
		}

		large = arrayJKLMN[neArr.length - 1];

		for (int i = 0; i < neArr.length; i++) {

			if (neArr[i] >= large) {
				large = neArr[i];
				if (large == neArr[i]) {
					largestList.add(i);
				}
			}

		}

		System.out.println("Largest List = " + largestList);
		return largestList;

	}

	public static List<Integer> lowest(int arrayJKLMN[]) {
		int small;

		List<Integer> smallestList = new ArrayList<>();

		Arrays.sort(arrayJKLMN);

		int neArr[] = new int[5];
		for (int i = 0; i < arrayJKLMN.length; i++) {
			neArr[i] = arrayJKLMN[i];
		}

		small = arrayJKLMN[0];
		for (int i = 0; i < neArr.length; i++) {

			if (neArr[i] <= small) {
				small = neArr[i];
				if (small == neArr[i]) {
					smallestList.add(i);
				}
			}

		}
		System.out.println("Smallest List = " + smallestList);
		return smallestList;

	}
}
