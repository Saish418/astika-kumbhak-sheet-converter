package com.sheetconvetor.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class HtmlToString {

	public static String convert(String jspUrl) {

		URL url;
		String inputLine = "";
		String newInputLine = "";

		try {
			// get URL content

			String a = jspUrl;
			url = new URL(a);
			URLConnection conn = url.openConnection();

			// open the stream and put it into BufferedReader
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			while ((inputLine = br.readLine()) != null) {

				if (inputLine != null) {
					newInputLine = newInputLine + inputLine;
				}

			}
			br.close();

			System.out.println("Done");
			return newInputLine;
		} catch (Exception e) {
			System.out.println("HtmlToString");
			e.printStackTrace();
			return newInputLine;
		}

	}

}
