package com.sheetconvetor.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.docx4j.convert.in.xhtml.XHTMLImporterImpl;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

public class HtmlToWord {

	public static String generate(String htmlString, HttpServletRequest request) {

		String path = "";
		try {

			// String base64 =
			// DatatypeConverter.printBase64Binary(Files.readAllBytes(Paths.get(String.valueOf("https://images.unsplash.com/photo-1618588507085-c79565432917?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YmVhdXRpZnVsJTIwbmF0dXJlfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80"))));

			System.out.println(request.getAttribute("q"));
			String xhtml = "<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" width=\"662\" style='margin-left: -5.4pt;'><tr><td style='border-bottom: none; border-right: none;'></td><td colspan=\"2\" style='border-bottom: none; border-right: none; border-left: none;'><br /><u>DIAGNOSIS FORM</u><br />"
					+ request.getAttribute("c")
					+ "</td><td style='border-bottom: none; border-left: none;'></td></tr><tr style='height: 15.75pt'><td colspan=\"4\" style='border-top: none;'><br /><span></span><img src='"
					+ request.getAttribute("q")
					+ "' width=\"50\" height=\"50\" allow=\"autoplay\" /></td></tr><tr style='height: 15.75pt'><td width=\"259\" valign=\"bottom\" style='padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>"
					+ request.getAttribute("a")
					+ "</td><td width=\"314\" colspan=\"2\" valign=\"bottom\" style='padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>"
					+ request.getAttribute("b")
					+ "</td><td width=\"89\" valign=\"bottom\" style='padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>"
					+ request.getAttribute("de")
					+ "</td></tr><tr style='height: 85.75pt'><td width=\"259\" valign=\"bottom\" style='padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 85.75pt'> "
					+ request.getAttribute("s")
					+ "</td><td width=\"314\" colspan=\"2\" valign=\"bottom\" style='padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 85.75pt'>Medicines: "
					+ request.getAttribute("t")
					+ "</td><td width=\"89\" valign=\"bottom\" style='padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 85.75pt'> PREGNANT : "
					+ request.getAttribute("r")
					+ "</td></tr><tr style='height: 15.75pt'><td width=\"259\" valign=\"bottom\" style='padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>"
					+ request.getAttribute("fghi")
					+ "</td><td width=\"157\" valign=\"bottom\" style='padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>"
					+ request.getAttribute("jklmn")
					+ "</td><td width=\"157\" valign=\"bottom\" style='padding: 5.0pt 5.0pt 5.0pt 5.0pt; height: 15.75pt'>"
					+ request.getAttribute("p")
					+ "</td><td width=\"89\" valign=\"bottom\" style='padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>"
					+ request.getAttribute("o") + "</td></tr></table>";

			// To docx, with content controls
			WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();

			XHTMLImporterImpl XHTMLImporter = new XHTMLImporterImpl(wordMLPackage);

			wordMLPackage.getMainDocumentPart().getContent().addAll(XHTMLImporter.convert(xhtml, null));

			String filePath = "D://sample_" + request.getAttribute("a") + ".docx";
			//String filePath = "C:\\Users\\Dexpert Garima\\git\\repository\\astika-kumbhak-sheet-converter\\src\\main\\resources\\mindfast\\" + request.getAttribute("a") + ".docx";

			wordMLPackage.save(new java.io.File(filePath));

			return filePath;
		} catch (Exception e) {
			System.out.println("HtmlToWord");
			e.printStackTrace();
			return null;
		}

	}

}
