<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<head>
<title>View</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Colored Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!-- bootstrap-css -->
<link rel="stylesheet" href="../css/bootstrap.css">
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="../css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->

<!-- font-awesome icons -->
<link rel="stylesheet" href="../css/font.css" type="text/css" />
<link href="../css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome icons -->
<script src="../js/jquery2.0.3.min.js"></script>
<script src="../js/modernizr.js"></script>
<script src="../js/jquery.cookie.js"></script>
<script src="../js/screenfull.js"></script>
<script>
	$(function() {
		$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

		if (!screenfull.enabled) {
			return false;
		}

		$('#toggle').click(function() {
			screenfull.toggle($('#container')[0]);
		});
	});
</script>
<!-- charts -->
<script src="../js/raphael-min.js"></script>
<script src="../js/morris.js"></script>
<link rel="stylesheet" href="../css/morris.css">
<!-- //charts -->
<!--skycons-icons-->
<link rel="stylesheet" type="text/css" href="../css/table-style.css" />
<link rel="stylesheet" type="text/css" href="../css/basictable.css" />
<script type="text/javascript" src="../js/jquery.basictable.min.js"></script>
<script src="../js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body class="dashboard-page">
	<!-- tables -->

	<script type="text/javascript">
		$(document).ready(function() {
			$('#table').basictable();

			$('#table-breakpoint').basictable({
				breakpoint : 768
			});

			$('#table-swap-axis').basictable({
				swapAxis : true
			});

			$('#table-force-off').basictable({
				forceResponsive : false
			});

			$('#table-no-resize').basictable({
				noResize : true
			});

			$('#table-two-axis').basictable();

			$('#table-max-height').basictable({
				tableWrapper : true
			});
		});
	</script>
	<script>
		var theme = $.cookie('protonTheme') || 'default';
		$('body').removeClass(function(index, css) {
			return (css.match(/\btheme-\S+/g) || []).join(' ');
		});
		if (theme !== 'default')
			$('body').addClass(theme);
	</script>
	<nav class="main-menu">
		<ul>

			<li class="has-subnav"><a href="javascript:;"> <i
					class="fa fa-list-ul" aria-hidden="true"></i> <span
					class="nav-text">View Treatment</span> <i class="icon-angle-right"></i><i
					class="icon-angle-down"></i>
			</a>

				<ul>
					<li><a class="subnav-text" href="faq.html">Payer Treatment
							History</a></li>
					<li><a class="subnav-text" href="blank.html">Blank Page</a></li>
				</ul></li>
		</ul>
		<ul class="logout">
			<li><a href="login.html"> <i class="icon-off nav-icon"></i>
					<span class="nav-text"> Logout </span>
			</a></li>
		</ul>
	</nav>
	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access"> <i
				class="icon-proton-logo"></i> <i class="icon-reorder"></i>
			</a>
		</nav>
		<section class="title-bar">
			<div class="logo">
				<h1>
					<a href="index.html"><img
						src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAADhCAMAAADmr0l2AAAArlBMVEX///8zZpkAAAAwZJg/bZ15lLUfXJMqYpYZWpK0xNdagquGoL3Fz92oqKjn6/FFRUXc4urz9vqbm5s6Ojpvb2+AgIC4uLh1dXW9zNtMTExWVlbAwMDk5OS5x8Xq6ur4+vxlZWXX19dNeKWrvdPPz89rjLGjo6MfHx89PT2htczL1+Xk6vKuv9PGxsYmJiYYGBiOjo5/nLtih6+UrMjT3egvLy8AUY2+zNEATowjIyMTpPtNAAANo0lEQVR4nO2ca2ObOhKGTYURJoljkjQlbTYkOKTxpU0d3O72//+xBcRFmhlhYeykPUfvNxsk9KCRNDOSPRpZWVlZWVlZWVlZWVlZWVlZWVlZWVlZWVlZWVlZWVlZWR1U03Q8UOkyryYbWktZ0/YQNZW1tJr94gPlT/JqUm9oNUVNUV5TMLQm/1QF9J2B8kpA3xuuXyUgG9qeowBuTw+h6Z8LeDgNB4ws4F8O+I83UQv47wNkvFymOCef/bcAMlnS/dzjbpotcmWp63n48X8JoBvISmpq7gXrWVjfHC+zxIcN+DsA+Yny7dwT3/LNEjYgCgDiLsBwtZqVWq3C7jtbwNqvLG2JdSCLG+q7+wJ66ZRqQuRwU8D4NAskL9VPs+UuyAKQpWVYkKZurgI3L4vQxLSQ257rvqZlgYQZAy7y+pija3mYytONHnA7DnyuvAzu+Um6jHcAMlf+Ig6n09XsFBCy19IopqFUmbjHFJC7yDrbZ2bSZKMDXKYuNe0y7gSTjm4sABPiezDbgwaXmvo9ALnbaUuTdiDSgOFG7TulcTzYUmV6AW6GAfrdfDIhCRglxIIiIy6GAo6HAXL9K65vqQcFARhnrtx9jJgHmY4QjcGjAE7UkIPUxtMBxidt97FiXkkSx4PjUdeH5TKBv44PC2iiOOAaQInP89LJLJ/oVsuNq86DjNFGogEMIWBKAJrPokbaik5BgCcNCfOyVfv4TB2WPCBXWVPAV+KeskE9AeNZ4Yaut9TalXkUYNTyJaqlR65C6GU9AKcQMDgM4DwRjoifLDBiSJnorKXgc1AgUqyUJdRMrQGEUQ87COCsneyZ5+IxU3YhABy3EHip2SiEnFpgilnUwS9zCwET4oX3BVz6ymQPMqq5prgHt1IJdwULhIqRUo0sARnu2iWMW4mFui/gFszszEd9uOEQcCy9Ew+a6Gi0VrrQJ1ajApDj6SeCvihxT0/AKVqemQPfWuHeKoBhIhUixsnMlSukFrNiDPLZTkBnOGCKfUnUoDg3OQVwrVgSMWwTxUYDbGcFoIfLHR6QTNYgI80tUgHcqKEitlHlBsbRKDUG9HEv9wMcU8EA8uHzyFgBVLudmEUm6jyKSd4KMKSDAQ5aPFMBYSkf9ZDaUGKtLwFxKDoxARR1/tcIcIlyBKJa8OiYMwUQ3o4A1cicmGVKQLQgjU7hiPGwdVeA/zECzOhw1YMxgNsNiGztFDgzyIY1gCNHNQ7SVXN7AL7SgMiJH3MFUPU2CTvaG3ChmhQVZosBYgYYJ/QYRLFopgDCqemAgNAJwnfUk4wRILS1tsmg0rXXsUw4v3aMQR0gFWfM5ZKUD1TNG2aAU3ofIq8ZtGiiAm7VOQQHbQAQe5RlXpQYYIpRkR1YzRtmgCvdnowPWhSpgOrY5fg9GwE6nEpZLpuqGbFQFjO6Yw6o3XSCE/8pAIykrueEJ2YISCQkJC+Cyhk2NjwUEEwbEHCUSRlTYqCYAVJLfRHVV1UTfuiocXOPDRieVBlfjgL6QpEZIJk5rJcKwsWVaj42YD7vFPstvvdKZryXZoD0NDlKeFmI3NyoR//QWRQATjDgaHp6Gp1SWyzx6UJdKPWAdBduixQKdBeFmjdnCKjpQDSLUoCk4unitdguVmrTA2rsMPPIKHkkzUCGnoyr60FQ79wIcLU8yY0We38dgHTObeRylFYo1S7AhtGEdicZ3LfhOwFX6xPHp02+AxD79QLEpx/XBqKG8eBG42xD3yTtBswNM3CJrjMAdDjZVfSuiTQpGgKu6XhwR7gENJ0kTDdb7Qb0emwnSA6UIeBME/ACHyn0tDu88TIfnzAzx4wnmeJh5HpOSc6amuZkyFkGTd4zHWAcbXywB8o833lNDZeJslH0fElI9oBNATOqC5GFLmjAOArUHezcUt1ksZzGhq5aVWjnNqyQUqkpINyvKp+IpmiYNhTavqrzCvOSoHrspAcguUdGSYnOjRO/C9yF3hq+BZdIHsRzcHaEB1nj1vQCJEN7LHVCNN+bCODs7qEgJoKp+wJ6rByFYiyQZ/Z+gHRgCxSDk1fGgCv1RJPjYd8Cb77kfCqCo24s9gM0OggHZose22dTeVud+Slyn6cMtSFM1aQFPE7UE5D2udVGOKp67fBmxemIQtxziXdJbIBuAACcB3sCava5ZcFNon5b2OF6nLiuG2wiIvqZcmRFa+VZzEGRTV9Ah+1Y7VEKvvcpizgXfWWDAGcg/4xff2/AHec/cAL3cMdIqncnA56oyx+x196/Bz28yyIJL2YHA5xWzpwEGKmeNXWYad0XsHu1h6dLjAHDzjOdheJ6cEuAYEPDJd79vDcgnWGrRGxDmwFOxrsIm/NMLeBW2YCnfeVFf8COpQLtGhoDRr/I3GqrdjloAUEMSaZnsx7RRPMEMj0zAkceegJ6XlcfxpK70gDC7V0qvw4mPTNAx9EsFRsy4jEEdLxXbbCyTaSaG0C4n0ECqlZsCEin6vGubz/A3BOlj3TGmRItNIBwQFCAq70AGXFwZqSJyXsAFu41TvGEC3DwswGEeSoKEAxTUxMlu5CI5/oC5h52ksknKcPTMYeHsRtAePCEAtz0ycnIhLgqXeq2F2D5+4skzSaFFicBJ/KbfQCh528MSOSftnQH9gV0yl/OiB+ucPJHNg0gPLdA+Fgw0TMEkJ5i9gHcoQbQBRfw9tkUWpU54K6t1DcBhHEA8mRQunwIIDpBenRAOAZZAuxqggawOSD2Ot4fEEbjW4dwMw0BPeKQ85ubKGGA8jQTlXzgHhdNHjQgQ3zvAIhde540hOGi4GN8osbE+Bz4HwxInK3hyWKVG1c4WyQivTGWTrsohbsBqfzo2wOOiOCFcRaMU5cJ94cnIThzhnc5aVeNiOpXmp++HhGQCj9LP8GpDxYUsbniQtYb1bPm+TQgETvrjtMdEZCMP2WVrvtK6UKe5oThPGk2koyd7XcA1J2irW+sVg3VX+PJySLxWdNHNCCVE4Oe0xsAxuOOYn69KoIfnLAyPmm8AhKQzG+/A2Ae6uuMlPntE+i91awLkAq9DxMu9QPMA1rdSWGpiVPqvDRL/wrA0dylxiF3ldRAhJy29uQeDQh3Xt8PsEhIIZfaT+EJN5WQeUGT3KUBqW3CdwIchVki79Azzwvw1lTkNPfkk0yyaH1uGpA6/vNegIXfOXab/68IMvJs0jQLRJ7AZ6n0Q1/dMnHwWVQXLhsBFu2PJuv5fD6J9H/OES4n5R3g58EaZztdT6Ko/v+1KJpMThLqPgf1dgXIEvUPAl93+CS7APeX5hQg93xZHvXXPWR76lNsTP2DQHO+P+8Pc2jAg1X4/oD/+D+Os4AWkACcHkTxnwv4+ssfrv8d5g9UISAbKPEXuHxoNUVNhQ8SDK0J/KRkxt2BYgXg2BlaTVFTAfg6tBbH4M9+rKysrKysrKysrKysrP4duj5rdX395fHhTr5693Cb605XWOhJFFa+i880erodPTzpLhYlHzVXRZZfLYsbjPXzA9T5WXv1qvzmU2cN36pyyoNiVG2t69GT9lpR8pPmmqj+grhy/qOL8ZIocdkUMAG8r0qdyV/G3/WAZ52AH+lLL3rAstJegG1vGADe1WV+viNgByENeG4O2Db34R0BP9z2A6ztzQDwpinzUQZ8OTBgxxgsddUNeF3+VDCOH+shfmkK+NA+40XezowrVTg3cf2NfO2D8nDxW0UBeN98J1+rAT+3dVw/Ky9AD1irnhNvDQHlF04OhAqQLEwV+9jZIQ1gq88dDx9hwNqAxDe7AX+L9uPnqvUdEfBW1PJDUwIBhuL+i/LDTkDR4Z/v5G5XdXTAqpaPmhIIsPpGvJCdgGIRPKv8hQvijuMDfu/Xg/H3HoCVw3JXeSdfiVveqgepl1sIAd72MVHR+vtmuf+mu+WIgNeilkdNCQR4JTd1F+B5U/pzjQq1H+DNty+K6jeHAR/Eivui+00nBKyn/dgE8KG20OY94tVoP0AkBfC5wr6+fqo9YZ2F1lPKw2Oub9c/agekYtoB+KPttspGn44E+KIAYp3jyhVApFsTwFjMncJ87jUPehPAS33ARAPWoU83YOX1KB8e4E1vAfix40fVJGBj0N2A6tXqWccB/K0FfLm56IzpCcDvXzQIQJXPU8/PomUv8IjhfoD3muZWgJeXP79Wre2OxhHg1/OrL1J/dwJWXuvjg9AF1dzjrYPxY+Vln3cnZQTgxd1tmV26A++/E5AevhDliAt9Zc0/tcFu20pdrNEFeEvyIY/7mJ5M1Ye/dyed9gH8oQEEXu8xAet8EF270N6AMU44CgGP+6i+aOU/Ef5Fo70Bv33QSfW4j+tsV0aqTVgMABSXbsTsJPQgpu575b7jAtZGeq+53wyQCpbv9I1TX+eRw6U6RadNjJoAXn5S9aOtWPWRHokRsR/gT/DIT1dPNGBtpN9NwyUKEKrIKd6QliGyAc/DAbHuNYD1amWcsjAAfG6q/QLur7wZ2eM+FOCVBrBxTzXL/Z6AYhH8Cu2iGpnyoD064KhK/WoWwz1NFIMIVYl8CfzYJtom12mI+/Ncl9DUal08nxO6Hz1elsVwouf6EtYnvqFnxRvi4ReX1CPPn8UYOyuuPoN1q2rl551/CGdlZWVlZWVlZWVlZWVlZWVlZWVlZWVlZWVlZWVlZWV1QP0fOEpLKrunSfEAAAAASUVORK5CYII="
						alt="" width="70" height="70" /></a>
				</h1>
			</div>
			<div class="full-screen">
				<section class="full-top">
					<button id="toggle">
						<i class="fa fa-arrows-alt" aria-hidden="true"></i>
					</button>
				</section>
			</div>
			<div class="w3l_search">
				<form action="#" method="post">
					<input type="text" name="search" value="Search"
						onfocus="this.value = '';"
						onblur="if (this.value == '') {this.value = 'Search';}"
						required="">
					<button class="btn btn-default" type="submit">
						<i class="fa fa-search" aria-hidden="true"></i>
					</button>
				</form>
			</div>
			<div class="header-right">
				<div class="profile_details_left">
					<div class="header-right-left">
						<!--notifications of menu start -->
						<ul class="nofitications-dropdown">
							<li class="dropdown head-dpdn"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown"><i
									class="fa fa-envelope"></i><span class="badge">3</span></a>
								<ul class="dropdown-menu anti-dropdown-menu w3l-msg">
									<li>
										<div class="notification_header">
											<h3>You have 3 new messages</h3>
										</div>
									</li>
									<li><a href="#">
											<div class="user_img">
												<img src="images/1.png" alt="">
											</div>
											<div class="notification_desc">
												<p>Lorem ipsum dolor amet</p>
												<p>
													<span>1 hour ago</span>
												</p>
											</div>
											<div class="clearfix"></div>
									</a></li>
									<li class="odd"><a href="#">
											<div class="user_img">
												<img src="images/2.png" alt="">
											</div>
											<div class="notification_desc">
												<p>Lorem ipsum dolor amet</p>
												<p>
													<span>1 hour ago</span>
												</p>
											</div>
											<div class="clearfix"></div>
									</a></li>
									<li><a href="#">
											<div class="user_img">
												<img src="images/3.png" alt="">
											</div>
											<div class="notification_desc">
												<p>Lorem ipsum dolor amet</p>
												<p>
													<span>1 hour ago</span>
												</p>
											</div>
											<div class="clearfix"></div>
									</a></li>
									<li>
										<div class="notification_bottom">
											<a href="#">See all messages</a>
										</div>
									</li>
								</ul></li>
							<li class="dropdown head-dpdn"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown"
								aria-expanded="false"><i class="fa fa-bell"></i><span
									class="badge blue">3</span></a>
								<ul class="dropdown-menu anti-dropdown-menu agile-notification">
									<li>
										<div class="notification_header">
											<h3>You have 3 new notifications</h3>
										</div>
									</li>
									<li><a href="#">
											<div class="user_img">
												<img src="images/2.png" alt="">
											</div>
											<div class="notification_desc">
												<p>Lorem ipsum dolor amet</p>
												<p>
													<span>1 hour ago</span>
												</p>
											</div>
											<div class="clearfix"></div>
									</a></li>
									<li class="odd"><a href="#">
											<div class="user_img">
												<img src="images/1.png" alt="">
											</div>
											<div class="notification_desc">
												<p>Lorem ipsum dolor amet</p>
												<p>
													<span>1 hour ago</span>
												</p>
											</div>
											<div class="clearfix"></div>
									</a></li>
									<li><a href="#">
											<div class="user_img">
												<img src="images/3.png" alt="">
											</div>
											<div class="notification_desc">
												<p>Lorem ipsum dolor amet</p>
												<p>
													<span>1 hour ago</span>
												</p>
											</div>
											<div class="clearfix"></div>
									</a></li>
									<li>
										<div class="notification_bottom">
											<a href="#">See all notifications</a>
										</div>
									</li>
								</ul></li>
							<li class="dropdown head-dpdn"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown"
								aria-expanded="false"><i class="fa fa-tasks"></i><span
									class="badge blue1">15</span></a>
								<ul class="dropdown-menu anti-dropdown-menu agile-task">
									<li>
										<div class="notification_header">
											<h3>You have 8 pending tasks</h3>
										</div>
									</li>
									<li><a href="#">
											<div class="task-info">
												<span class="task-desc">Database update</span><span
													class="percentage">40%</span>
												<div class="clearfix"></div>
											</div>
											<div class="progress progress-striped active">
												<div class="bar yellow" style="width: 40%;"></div>
											</div>
									</a></li>
									<li><a href="#">
											<div class="task-info">
												<span class="task-desc">Dashboard done</span><span
													class="percentage">90%</span>
												<div class="clearfix"></div>
											</div>
											<div class="progress progress-striped active">
												<div class="bar green" style="width: 90%;"></div>
											</div>
									</a></li>
									<li><a href="#">
											<div class="task-info">
												<span class="task-desc">Mobile App</span><span
													class="percentage">33%</span>
												<div class="clearfix"></div>
											</div>
											<div class="progress progress-striped active">
												<div class="bar red" style="width: 33%;"></div>
											</div>
									</a></li>
									<li><a href="#">
											<div class="task-info">
												<span class="task-desc">Issues fixed</span><span
													class="percentage">80%</span>
												<div class="clearfix"></div>
											</div>
											<div class="progress progress-striped active">
												<div class="bar  blue" style="width: 80%;"></div>
											</div>
									</a></li>
									<li>
										<div class="notification_bottom">
											<a href="#">See all pending tasks</a>
										</div>
									</li>
								</ul></li>
							<div class="clearfix"></div>
						</ul>
					</div>
					<div class="profile_details">
						<ul>
							<li class="dropdown profile_details_drop"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown"
								aria-expanded="false">
									<div class="profile_img">
										<span class="prfil-img"><i class="fa fa-user"
											aria-hidden="true"></i></span>
										<div class="clearfix"></div>
									</div>
							</a>
								<ul class="dropdown-menu drp-mnu">
									<li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
									<li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
									<li><a href="#"><i class="fa fa-sign-out"></i> Logout</a>
									</li>
								</ul></li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</section>
		<!-- <div class="main-grid"> -->

		<!-- <div class="social grid"> -->
		<!-- <div class="grid-info"> -->
		<!-- <div class="col-md-3 top-comment-grid"> -->
		<!-- <div class="comments likes"> -->
		<!-- <div class="comments-icon"> -->
		<!-- <i class="fa fa-facebook"></i> -->
		<!-- </div> -->
		<!-- <div class="comments-info likes-info"> -->
		<!-- <h3>95K</h3> -->
		<!-- <a href="#">Likes</a> -->
		<!-- </div> -->
		<!-- <div class="clearfix"> </div> -->
		<!-- </div> -->
		<!-- </div> -->
		<!-- <div class="col-md-3 top-comment-grid"> -->
		<!-- <div class="comments"> -->
		<!-- <div class="comments-icon"> -->
		<!-- <i class="fa fa-comments"></i> -->
		<!-- </div> -->
		<!-- <div class="comments-info"> -->
		<!-- <h3>12K</h3> -->
		<!-- <a href="#">Comments</a> -->
		<!-- </div> -->
		<!-- <div class="clearfix"> </div> -->
		<!-- </div> -->
		<!-- </div> -->
		<!-- <div class="col-md-3 top-comment-grid"> -->
		<!-- <div class="comments tweets"> -->
		<!-- <div class="comments-icon"> -->
		<!-- <i class="fa fa-twitter"></i> -->
		<!-- </div> -->
		<!-- <div class="comments-info tweets-info"> -->
		<!-- <h3>27K</h3> -->
		<!-- <a href="#">Tweets</a> -->
		<!-- </div> -->
		<!-- <div class="clearfix"> </div> -->
		<!-- </div> -->
		<!-- </div> -->
		<!-- <div class="col-md-3 top-comment-grid"> -->
		<!-- <div class="comments views"> -->
		<!-- <div class="comments-icon"> -->
		<!-- <i class="fa fa-eye"></i> -->
		<!-- </div> -->
		<!-- <div class="comments-info views-info"> -->
		<!-- <h3>557K</h3> -->
		<!-- <a href="#">Views</a> -->
		<!-- </div> -->
		<!-- <div class="clearfix"> </div> -->
		<!-- </div> -->
		<!-- </div> -->
		<!-- <div class="clearfix"> </div> -->
		<!-- </div> -->
		<!-- </div> -->

		<!-- <div class="agile-grids"> -->
		<!-- <div class="col-md-4 charts-right"> -->

		<!-- <div class="area-grids"> -->
		<!-- <div class="area-grids-heading"> -->
		<!-- <h3>Area charts</h3> -->
		<!-- </div> -->
		<!-- <div id="graph4"></div> -->
		<!-- <script> -->
		<!-- Morris.Donut({ -->
		<!-- element: 'graph4', -->
		<!-- data: [ -->
		<!-- {value: 70, label: 'foo', formatted: 'at least 70%' }, -->
		<!-- {value: 15, label: 'bar', formatted: 'approx. 15%' }, -->
		<!-- {value: 10, label: 'baz', formatted: 'approx. 10%' }, -->
		<!-- {value: 5, label: 'A really really long label', formatted: 'at most 5%' } -->
		<!-- ], -->
		<!-- formatter: function (x, data) { return data.formatted; } -->
		<!-- }); -->
		<!-- </script> -->

		<!-- </div> -->

		<!-- </div> -->
		<!-- <div class="col-md-8 chart-left"> -->

		<!-- <div class="agile-Updating-grids"> -->
		<!-- <div class="area-grids-heading"> -->
		<!-- <h3>Updating data</h3> -->
		<!-- </div> -->
		<!-- <div id="graph1"></div> -->
		<!-- <script> -->
		<!-- var nReloads = 0; -->
		<!-- function data(offset) { -->
		<!-- var ret = []; -->
		<!-- for (var x = 0; x <= 360; x += 10) { -->
		<!-- var v = (offset + x) % 360; -->
		<!-- ret.push({ -->
		<!-- x: x, -->
		<!-- y: Math.sin(Math.PI * v / 180).toFixed(4), -->
		<!-- z: Math.cos(Math.PI * v / 180).toFixed(4) -->
		<!-- }); -->
		<!-- } -->
		<!-- return ret; -->
		<!-- } -->
		<!-- var graph = Morris.Line({ -->
		<!-- element: 'graph1', -->
		<!-- data: data(0), -->
		<!-- xkey: 'x', -->
		<!-- ykeys: ['y', 'z'], -->
		<!-- labels: ['sin()', 'cos()'], -->
		<!-- parseTime: false, -->
		<!-- ymin: -1.0, -->
		<!-- ymax: 1.0, -->
		<!-- hideHover: true -->
		<!-- }); -->
		<!-- function update() { -->
		<!-- nReloads++; -->
		<!-- graph.setData(data(5 * nReloads)); -->
		<!-- $('#reloadStatus').text(nReloads + ' reloads'); -->
		<!-- } -->
		<!-- setInterval(update, 100); -->
		<!-- </script> -->

		<!-- </div> -->

		<!-- </div> -->
		<!-- <div class="clearfix"> </div> -->
		<!-- </div> -->

		<!-- <div class="agile-bottom-grids"> -->
		<!-- <div class="col-md-6 agile-bottom-right"> -->
		<!-- <div class="agile-bottom-grid"> -->
		<!-- <div class="area-grids-heading"> -->
		<!-- <h3>Stacked Bars chart</h3> -->
		<!-- </div> -->
		<!-- <div id="graph6"></div> -->
		<!-- <script> -->
		<!-- // Use Morris.Bar -->
		<!-- Morris.Bar({ -->
		<!-- element: 'graph6', -->
		<!-- data: [ -->
		<!-- {x: '2011 Q1', y: 0}, -->
		<!-- {x: '2011 Q2', y: 1}, -->
		<!-- {x: '2011 Q3', y: 2}, -->
		<!-- {x: '2011 Q4', y: 3}, -->
		<!-- {x: '2012 Q1', y: 4}, -->
		<!-- {x: '2012 Q2', y: 5}, -->
		<!-- {x: '2012 Q3', y: 6}, -->
		<!-- {x: '2012 Q4', y: 7}, -->
		<!-- {x: '2013 Q1', y: 8} -->
		<!-- ], -->
		<!-- xkey: 'x', -->
		<!-- ykeys: ['y'], -->
		<!-- labels: ['Y'], -->
		<!-- barColors: function (row, series, type) { -->
		<!-- if (type === 'bar') { -->
		<!-- var red = Math.ceil(255 * row.y / this.ymax); -->
		<!-- return 'rgb(' + red + ',0,0)'; -->
		<!-- } -->
		<!-- else { -->
		<!-- return '#000'; -->
		<!-- } -->
		<!-- } -->
		<!-- }); -->
		<!-- </script> -->
		<!-- </div> -->
		<!-- </div> -->
		<!-- <div class="col-md-6 agile-bottom-left"> -->
		<!-- <div class="agile-bottom-grid"> -->
		<!-- <div class="area-grids-heading"> -->
		<!-- <h3>Stacked Bars chart</h3> -->
		<!-- </div> -->
		<!-- <div id="graph5"></div> -->
		<!-- <script> -->
		<!-- // Use Morris.Bar -->
		<!-- Morris.Bar({ -->
		<!-- element: 'graph5', -->
		<!-- data: [ -->
		<!-- {x: '2011 Q1', y: 3, z: 2, a: 3}, -->
		<!-- {x: '2011 Q2', y: 2, z: null, a: 1}, -->
		<!-- {x: '2011 Q3', y: 0, z: 2, a: 4}, -->
		<!-- {x: '2011 Q4', y: 2, z: 4, a: 3} -->
		<!-- ], -->
		<!-- xkey: 'x', -->
		<!-- ykeys: ['y', 'z', 'a'], -->
		<!-- labels: ['Y', 'Z', 'A'], -->
		<!-- stacked: true -->
		<!-- }); -->
		<!-- </script> -->
		<!-- </div> -->
		<!-- </div> -->
		<!-- <div class="clearfix"> </div> -->
		<!-- </div> -->
		<!-- <div class="agile-last-grids"> -->
		<!-- <div class="col-md-4 agile-last-left"> -->
		<!-- <div class="agile-last-grid"> -->
		<!-- <div class="area-grids-heading"> -->
		<!-- <h3>Daylight savings time</h3> -->
		<!-- </div> -->
		<!-- <div id="graph7"></div> -->
		<!-- <script> -->
		<!-- // This crosses a DST boundary in the UK. -->
		<!-- Morris.Area({ -->
		<!-- element: 'graph7', -->
		<!-- data: [ -->
		<!-- {x: '2013-03-30 22:00:00', y: 3, z: 3}, -->
		<!-- {x: '2013-03-31 00:00:00', y: 2, z: 0}, -->
		<!-- {x: '2013-03-31 02:00:00', y: 0, z: 2}, -->
		<!-- {x: '2013-03-31 04:00:00', y: 4, z: 4} -->
		<!-- ], -->
		<!-- xkey: 'x', -->
		<!-- ykeys: ['y', 'z'], -->
		<!-- labels: ['Y', 'Z'] -->
		<!-- }); -->
		<!-- </script> -->

		<!-- </div> -->
		<!-- </div> -->
		<!-- <div class="col-md-4 agile-last-left agile-last-middle"> -->
		<!-- <div class="agile-last-grid"> -->
		<!-- <div class="area-grids-heading"> -->
		<!-- <h3>Daylight savings time</h3> -->
		<!-- </div> -->
		<!-- <div id="graph8"></div> -->
		<!-- <script> -->
		<!-- /* data stolen from http://howmanyleft.co.uk/vehicle/jaguar_'e'_type */ -->
		<!-- var day_data = [ -->
		<!-- {"period": "2012-10-01", "licensed": 3407, "sorned": 660}, -->
		<!-- {"period": "2012-09-30", "licensed": 3351, "sorned": 629}, -->
		<!-- {"period": "2012-09-29", "licensed": 3269, "sorned": 618}, -->
		<!-- {"period": "2012-09-20", "licensed": 3246, "sorned": 661}, -->
		<!-- {"period": "2012-09-19", "licensed": 3257, "sorned": 667}, -->
		<!-- {"period": "2012-09-18", "licensed": 3248, "sorned": 627}, -->
		<!-- {"period": "2012-09-17", "licensed": 3171, "sorned": 660}, -->
		<!-- {"period": "2012-09-16", "licensed": 3171, "sorned": 676}, -->
		<!-- {"period": "2012-09-15", "licensed": 3201, "sorned": 656}, -->
		<!-- {"period": "2012-09-10", "licensed": 3215, "sorned": 622} -->
		<!-- ]; -->
		<!-- Morris.Bar({ -->
		<!-- element: 'graph8', -->
		<!-- data: day_data, -->
		<!-- xkey: 'period', -->
		<!-- ykeys: ['licensed', 'sorned'], -->
		<!-- labels: ['Licensed', 'SORN'], -->
		<!-- xLabelAngle: 60 -->
		<!-- }); -->
		<!-- </script> -->
		<!-- </div> -->
		<!-- </div> -->
		<!-- <div class="col-md-4 agile-last-left agile-last-right"> -->
		<!-- <div class="agile-last-grid"> -->
		<!-- <div class="area-grids-heading"> -->
		<!-- <h3>Daylight savings time</h3> -->
		<!-- </div> -->
		<!-- <div id="graph9"></div> -->
		<!-- <script> -->
		<!-- var day_data = [ -->
		<!-- {"elapsed": "I", "value": 34}, -->
		<!-- {"elapsed": "II", "value": 24}, -->
		<!-- {"elapsed": "III", "value": 3}, -->
		<!-- {"elapsed": "IV", "value": 12}, -->
		<!-- {"elapsed": "V", "value": 13}, -->
		<!-- {"elapsed": "VI", "value": 22}, -->
		<!-- {"elapsed": "VII", "value": 5}, -->
		<!-- {"elapsed": "VIII", "value": 26}, -->
		<!-- {"elapsed": "IX", "value": 12}, -->
		<!-- {"elapsed": "X", "value": 19} -->
		<!-- ]; -->
		<!-- Morris.Line({ -->
		<!-- element: 'graph9', -->
		<!-- data: day_data, -->
		<!-- xkey: 'elapsed', -->
		<!-- ykeys: ['value'], -->
		<!-- labels: ['value'], -->
		<!-- parseTime: false -->
		<!-- }); -->
		<!-- </script> -->

		<!-- </div> -->
		<!-- </div> -->
		<!-- <div class="clearfix"> </div> -->
		<!-- </div> -->
		<!-- <div class="agile-two-grids"> -->
		<!-- <div class="col-md-6 count"> -->
		<!-- <div class="count-grid"> -->
		<!-- <h3 class="title">Countdown</h3> -->
		<!-- <ul id="example"> -->
		<!-- <li><span class="hours">00</span><p class="hours_text">Hours</p></li> -->
		<!-- <li class="seperator">:</li> -->
		<!-- <li><span class="minutes">00</span><p class="minutes_text">Minutes</p></li> -->
		<!-- <li class="seperator">:</li> -->
		<!-- <li><span class="seconds">00</span><p class="seconds_text">Seconds</p></li> -->
		<!-- </ul> -->
		<!-- <div class="clearfix"> </div> -->
		<!-- <script type="text/javascript" src="js/jquery.countdown.min.js"></script> -->
		<!-- <script type="text/javascript"> -->
		<!-- $('#example').countdown({ -->
		<!-- date: '12/24/2020 18:59:59', -->
		<!-- offset: -8, -->
		<!-- day: 'Day', -->
		<!-- days: 'Days' -->
		<!-- }, function () { -->
		<!-- alert('Done!'); -->
		<!-- }); -->
		<!-- </script> -->
		<!-- </div> -->
		<!-- </div> -->
		<!-- <div class="col-md-6 weather"> -->
		<!-- <div class="weather-right"> -->
		<!-- <div class="weather-heading"> -->
		<!-- <h3>Weather Report</h3> -->
		<!-- </div> -->
		<!-- <ul> -->
		<!-- <li> -->
		<!-- <figure class="icons"> -->
		<!-- <canvas id="partly-cloudy-day" width="60" height="60"></canvas> -->
		<!-- </figure> -->
		<!-- <h3>25 �C</h3> -->
		<!-- <div class="clearfix"></div> -->
		<!-- </li> -->
		<!-- <li> -->
		<!-- <figure class="icons"> -->
		<!-- <canvas id="clear-day" width="40" height="40"></canvas> -->
		<!-- </figure> -->
		<!-- <div class="weather-text"> -->
		<!-- <h4>WED</h4> -->
		<!-- <h5>27 �C</h5> -->
		<!-- </div> -->
		<!-- <div class="clearfix"></div> -->
		<!-- </li> -->
		<!-- <li> -->
		<!-- <figure class="icons"> -->
		<!-- <canvas id="snow" width="40" height="40"></canvas> -->
		<!-- </figure> -->
		<!-- <div class="weather-text"> -->
		<!-- <h4>THR</h4> -->
		<!-- <h5>13 �C</h5> -->
		<!-- </div> -->
		<!-- <div class="clearfix"></div> -->
		<!-- </li> -->
		<!-- <li> -->
		<!-- <figure class="icons"> -->
		<!-- <canvas id="partly-cloudy-night" width="40" height="40"></canvas> -->
		<!-- </figure> -->
		<!-- <div class="weather-text"> -->
		<!-- <h4>FRI</h4> -->
		<!-- <h5>18 �C</h5> -->
		<!-- </div> -->
		<!-- <div class="clearfix"></div> -->
		<!-- </li> -->
		<!-- <li> -->
		<!-- <figure class="icons"> -->
		<!-- <canvas id="cloudy" width="40" height="40"></canvas> -->
		<!-- </figure> -->
		<!-- <div class="weather-text"> -->
		<!-- <h4>SAT</h4> -->
		<!-- <h5>15 �C</h5> -->
		<!-- </div> -->
		<!-- <div class="clearfix"></div> -->
		<!-- </li> -->
		<!-- <li> -->
		<!-- <figure class="icons"> -->
		<!-- <canvas id="fog" width="40" height="40"></canvas> -->
		<!-- </figure> -->
		<!-- <div class="weather-text"> -->
		<!-- <h4>SUN</h4> -->
		<!-- <h5>11 �C</h5> -->
		<!-- </div> -->
		<!-- <div class="clearfix"></div> -->
		<!-- </li> -->
		<!-- </ul> -->
		<!-- <script> -->
		<!-- var icons = new Skycons({"color": "#fcb216"}), -->
		<!-- list  = [ -->
		<!-- "partly-cloudy-day" -->
		<!-- ], -->
		<!-- i; -->

		<!-- for(i = list.length; i--; ) -->
		<!-- icons.set(list[i], list[i]); -->
		<!-- icons.play(); -->
		<!-- </script> -->
		<!-- <script> -->
		<!-- var icons = new Skycons({"color": "#000"}), -->
		<!-- list  = [ -->
		<!-- "clear-night","partly-cloudy-night", "cloudy", "clear-day", "sleet", "snow", "wind","fog" -->
		<!-- ], -->
		<!-- i; -->

		<!-- for(i = list.length; i--; ) -->
		<!-- icons.set(list[i], list[i]); -->
		<!-- icons.play(); -->
		<!-- </script> -->
		<!-- </div> -->
		<!-- </div> -->
		<!-- <div class="clearfix"> </div> -->
		<!-- </div> -->
		<!-- </div> -->
		<!-- footer -->
		<center>
			<button class="btn btn-primary btn" onClick="window.location.reload();">Reload</button>
		</center>

		<%-- <table id="table-no-resize">
			<thead>
				<tr>
				    <th>Sr No.</th>
					<th>Name</th>
					<th>Age</th>
					<th>Gender</th>
					<th>Height</th>
					<th>Email</th>
					<th>Mobile</th>
					<th>Document</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${list}" var="client" varStatus="loop">


					<tr>
						<td>${(loop.index)+1}</td>
						<td>${client.name}</td>
						<td>${client.age}</td>
						<td>${client.gender}</td>
						<td>${client.height}</td>
						<td>${client.email}</td>
						<td>${client.mobile}</td>
						<td><a class="btn btn-primary btn-sm" href="/mindfast/Sanjay Kumar.docx" download>Download</a></td>
					</tr>
				</c:forEach>

			</tbody>
		</table> --%>

		<!-- //footer -->
	</section>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/proton.js"></script>
</body>
</html>
