<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!-- /* Font Definitions */
@font-face {
	font-family: "Cambria Math";
	panose-1: 2 4 5 3 5 4 6 3 2 4;
}

@font-face {
	font-family: Roboto;
	panose-1: 0 0 0 0 0 0 0 0 0 0;
}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal {
	margin: 0in;
	line-height: 115%;
	font-size: 11.0pt;
	font-family: "Arial", sans-serif;
}

.MsoChpDefault {
	font-family: "Arial", sans-serif;
}

.MsoPapDefault {
	line-height: 115%;
}

@page WordSection1 {
	size: 8.5in 11.0in;
	margin: 1.0in 1.0in 1.0in 1.0in;
}

div.WordSection1 {
	page: WordSection1;
}
/* List Definitions */
ol {
	margin-bottom: 0in;
}

ul {
	margin-bottom: 0in;
}
-->
</style>

</head>

<body>

	<div class=WordSection1>

		<p class=MsoNormal>
			<span >                                                                       
				<b><u>DIAGNOSIS FORM</u></b>
			</span>
		</p>

		<p class=MsoNormal style='margin-left: 2.5in; text-indent: .5in'>
			<span  style="text-transform: uppercase;">${c} <b><span
					style='color: #0070C0'>C</span></b></span>
		</p>

		<p class=MsoNormal>
			<span><img width=177 height=176
				src="${q}"></span><span
				 style='font-size: 72.0pt; line-height: 115%; color: #0070C0'>
				Q</span>
		</p>

		<table class=42 border=1 cellspacing=0 cellpadding=0 width=662
			style='margin-left: -5.4pt; border-collapse: collapse; border: none'>
			<tr style='height: 15.75pt'>
				<td width=259 valign=bottom
					style='width: 194.25pt; border: solid #CCCCCC 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>${a}<span style='color: #0070C0'>A</span>
						</span>
					</p>
				</td>
				<td width=314 colspan=2 valign=bottom
					style='width: 235.5pt; border: solid #CCCCCC 1.0pt; border-left: none; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>${b}</span><span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>67</span><span
							
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto'>       
							<span style='color: #0070C0'>B</span>
						</span>
					</p>
				</td>
				<td width=89 valign=bottom
					style='width: 66.75pt; border: solid #CCCCCC 1.0pt; border-left: none; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>${de}</span>
					</p>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; color: #0070C0'>D 
							/  E</span><span  style='font-size: 10.0pt; line-height: 115%'>
						</span>
					</p>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>&nbsp;</span>
					</p>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; color: #00B050'></span>
					</p>
				</td>
			</tr>
			<tr style='height: 85.75pt'>
				<td width=259 valign=bottom
					style='width: 194.25pt; border-top: none; border-left: solid #CCCCCC 1.0pt; border-bottom: solid #CCCCCC 1.0pt; border-right: solid black 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 85.75pt'>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>${s}</span>
					</p>
				
					<p class=MsoNormal>
						<b><span 
							style='font-size: 12.0pt; line-height: 115%; color: #0070C0'>S</span></b>
					</p>
				</td>
				<td width=314 colspan=2 valign=bottom
					style='width: 235.5pt; border-top: none; border-left: none; border-bottom: solid #CCCCCC 1.0pt; border-right: solid black 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 85.75pt'>
					<p class=MsoNormal>
						<b><span  style='font-size: 10.0pt; line-height: 115%'>Medicines:</span></b><b><span
							
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>${t}</span></b>
					</p>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'><br>
						</span><b><span 
							style='font-size: 12.0pt; line-height: 115%; color: #0070C0'>T</span></b>
					</p>
				</td>
				<td width=89 valign=bottom
					style='width: 66.75pt; border-top: none; border-left: none; border-bottom: solid #CCCCCC 1.0pt; border-right: solid #CCCCCC 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 85.75pt'>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>PREGNANT
							${r}<b><span style='color: #0070C0'>R</span></b>
						</span>
					</p>
					<p class=MsoNormal>
						<b><span 
							style='font-size: 10.0pt; line-height: 115%; color: #0070C0'>&nbsp;</span></b>
					</p>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>&nbsp;</span>
					</p>
				</td>
			</tr>
			<tr style='height: 15.75pt'>
				<td width=259 valign=bottom
					style='width: 194.25pt; border: solid #CCCCCC 1.0pt; border-top: none; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>${fghi} <b><span style='color: #0070C0'> </span></b>
						</span>
					</p>
					<p class=MsoNormal>
						<b><span 
							style='font-size: 10.0pt; line-height: 115%; color: #0070C0'>&nbsp;</span></b>
					</p>
					<p class=MsoNormal>
						<b><span 
							style='font-size: 10.0pt; line-height: 115%; color: #0070C0'>F:G:H:I</span></b>
					</p>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>&nbsp;</span>
					</p>
				</td>
				<td width=157 valign=bottom
					style='width: 117.75pt; border-top: none; border-left: none; border-bottom: solid #CCCCCC 1.0pt; border-right: solid #CCCCCC 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>${jklmn}</span>
					</p>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>&nbsp;</span>
					</p>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>   
						</span>
					</p>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'> 
							 Lowest Code &amp; Highest Code from Col. <b><span
								style='color: #0070C0'>JKLMN</span></b>
						</span>
					</p>
					<p class=MsoNormal>
						<b><span 
							style='font-size: 10.0pt; line-height: 115%; color: #0070C0'>&nbsp;</span></b>
					</p>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; color: #00B050'></span><span 
							style='font-size: 10.0pt; line-height: 115%'>                            </span>
					</p>
				</td>
				<td width=157 valign=bottom
					style='width: 117.75pt; border-top: none; border-left: none; border-bottom: solid #CCCCCC 1.0pt; border-right: solid #CCCCCC 1.0pt; padding: 5.0pt 5.0pt 5.0pt 5.0pt; height: 15.75pt'>
					<p class=MsoNormal align=center style='text-align: center'>
						<span  style='font-size: 10.0pt; line-height: 115%'>${p}</span>
					</p>
					<p class=MsoNormal align=right style='text-align: right'>
						<span  style='font-size: 10.0pt; line-height: 115%'>&nbsp;</span>
					</p>
					<p class=MsoNormal align=right style='text-align: right'>
						<span  style='font-size: 10.0pt; line-height: 115%'>&nbsp;</span>
					</p>
					<p class=MsoNormal align=right style='text-align: right'>
						<b><span 
							style='font-size: 10.0pt; line-height: 115%; color: #0070C0'>P</span></b>
					</p>
					<p class=MsoNormal align=right style='text-align: right'>
						<span 
							style='font-size: 10.0pt; line-height: 115%; color: #00B050'></span>
					</p>
				</td>
				<td width=89 valign=bottom
					style='width: 66.75pt; border-top: none; border-left: none; border-bottom: solid #CCCCCC 1.0pt; border-right: solid #CCCCCC 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>${o}</span>
					</p>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto'>&nbsp;</span>
					</p>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto'>&nbsp;</span>
					</p>
					<p class=MsoNormal>
						<b><span 
							style='font-size: 10.0pt; line-height: 115%; color: #0070C0'>O</span></b>
					</p>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; color: #00B050'></span>
					</p>
				</td>
			</tr>
			<tr style='height: 15.75pt'>
				<td width=259 valign=bottom
					style='width: 194.25pt; border: solid #CCCCCC 1.0pt; border-top: none; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>&nbsp;</span>
					</p>
				</td>
				<td width=157 valign=bottom
					style='width: 117.75pt; border-top: none; border-left: none; border-bottom: solid #CCCCCC 1.0pt; border-right: solid #CCCCCC 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>&nbsp;</span>
					</p>
				</td>
				<td width=157 valign=bottom
					style='width: 117.75pt; border-top: none; border-left: none; border-bottom: solid #CCCCCC 1.0pt; border-right: solid #CCCCCC 1.0pt; padding: 5.0pt 5.0pt 5.0pt 5.0pt; height: 15.75pt'>
					<p class=MsoNormal align=right style='text-align: right'>
						<span  style='font-size: 10.0pt; line-height: 115%'>&nbsp;</span>
					</p>
				</td>
				<td width=89 valign=bottom
					style='width: 66.75pt; border-top: none; border-left: none; border-bottom: solid #CCCCCC 1.0pt; border-right: solid #CCCCCC 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>&nbsp;</span>
					</p>
				</td>
			</tr>
		</table>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span  style='background: yellow'>Diagnosed Problem
				Area: S-A</span>
		</p>

		<p class=MsoNormal>
			<span  style='background: yellow'>Tattva Dosh: J</span>
		</p>

		<p class=MsoNormal>
			<span  style='background: yellow'>Prakriti Dosh: V-P</span>
		</p>

		<p class=MsoNormal>
			<span  style='background: yellow'>Pathology /Effect :
				Weak Metabolic and Distributive Aspect, Decreased factor of
				Relaxation</span><span > </span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<span 
			style='font-size: 11.0pt; line-height: 115%; font-family: "Arial", sans-serif'><br
			clear=all style='page-break-before: always'> </span>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span><img width=177 height=176 id=image35.jpg
				src="Output%20Report_From%20Google%20Form%20Output-shared%20(1)_files/image002.jpg"></span>
		</p>

		<table class=42 border=1 cellspacing=0 cellpadding=0 width=662
			style='margin-left: -5.4pt; border-collapse: collapse; border: none'>
			<tr style='height: 15.75pt'>
				<td width=259 valign=bottom
					style='width: 194.25pt; border: solid #CCCCCC 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>Vijay
							Laxmi</span>
					</p>
				</td>
				<td width=314 valign=bottom
					style='width: 235.5pt; border: solid #CCCCCC 1.0pt; border-left: none; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>F/
							52 / 5.2 / </span><span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>67</span>
					</p>
				</td>
				<td width=89 valign=bottom
					style='width: 66.75pt; border: solid #CCCCCC 1.0pt; border-left: none; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>M,
							D  / B</span>
					</p>
				</td>
			</tr>
			<tr style='height: 33.0pt'>
				<td width=259 valign=bottom
					style='width: 194.25pt; border-top: none; border-left: solid #CCCCCC 1.0pt; border-bottom: solid #CCCCCC 1.0pt; border-right: solid black 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 33.0pt'>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>1.
							Left Hand Shoulder (LHS) Ligment tire Opertion done 3 years back.
							Now Doctor says it to be done again. She is scare to do it.</span>
					</p>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>2.
							Right Hand Side Knee(RHK) joint pain. Not able to flod leg
							properly.</span>
					</p>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>3.
							Acidity Problem</span>
					</p>
				</td>
				<td width=314 valign=bottom
					style='width: 235.5pt; border-top: none; border-left: none; border-bottom: solid #CCCCCC 1.0pt; border-right: solid black 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 33.0pt'>
					<p class=MsoNormal>
						<b><span  style='font-size: 10.0pt; line-height: 115%'>Medicines:</span></b><b><span
							
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>Thyrox
								50mg</span></b><span  style='font-size: 10.0pt; line-height: 115%'><br>
							<br> </span>
					</p>
				</td>
				<td width=89 valign=bottom
					style='width: 66.75pt; border-top: none; border-left: none; border-bottom: solid #CCCCCC 1.0pt; border-right: solid #CCCCCC 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 33.0pt'>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>&nbsp;</span>
					</p>
				</td>
			</tr>
			<tr style='height: 15.75pt'>
				<td width=259 valign=bottom
					style='width: 194.25pt; border: solid #CCCCCC 1.0pt; border-top: none; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>5
							: 5 : 14 : 25</span>
					</p>
				</td>
				<td width=314 valign=bottom
					style='width: 235.5pt; border-top: none; border-left: none; border-bottom: solid #CCCCCC 1.0pt; border-right: solid #CCCCCC 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span  style='font-size: 10.0pt; line-height: 115%'>,<span
							style='background: yellow'>Ak -U / Ag, V -O</span>                                    
							Pitta
						</span>
					</p>
				</td>
				<td width=89 valign=bottom
					style='width: 66.75pt; border-top: none; border-left: none; border-bottom: solid #CCCCCC 1.0pt; border-right: solid #CCCCCC 1.0pt; padding: 2.0pt 2.0pt 2.0pt 2.0pt; height: 15.75pt'>
					<p class=MsoNormal>
						<span 
							style='font-size: 10.0pt; line-height: 115%; font-family: Roboto; background: white'>Salty</span>
					</p>
				</td>
			</tr>
		</table>

		<p class=MsoNormal>
			<span >Diagnosed Problem Area: S-A</span>
		</p>

		<p class=MsoNormal>
			<span >Tattva Dosh: J</span>
		</p>

		<p class=MsoNormal>
			<span >Prakriti Dosh: V-P</span>
		</p>

		<p class=MsoNormal>
			<span >Pathology /Effect : Weak Metabolic and
				Distributive Aspect, Decreased factor of Relaxation </span>
		</p>

		<p class=MsoNormal>
			<span >Customised individual practice: Udgami Kumbhak :
				1 round/hour</span>
		</p>

		<p class=MsoNormal>
			<span >Additional practice:Sheetkari - 21 rounds- twice</span>
		</p>

		<ol style='margin-top: 0in' start=1 type=1>
			<li class=MsoNormal><span >Morning Preparatory : </span></li>
		</ol>

		<p class=MsoNormal style='margin-left: 1.0in; text-indent: -.25in'>
			<span >●<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
			</span></span><span >BHUMIKARAN KUMBHAK: </span>
		</p>

		<p class=MsoNormal style='margin-left: 1.0in; text-indent: -.25in'>
			<span >●<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
			</span></span><span >RUDRA VYANA KUMBHAK : 6 rounds, no hold after
				exhalation</span>
		</p>

		<p class=MsoNormal>
			<span >     2.  Morning Practice:</span>
		</p>

		<p class=MsoNormal style='margin-left: 1.0in; text-indent: -.25in'>
			<span >●<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
			</span></span><span >Somagni Kumbhak:  Exhale from both</span>
		</p>

		<p class=MsoNormal style='margin-left: 1.0in; text-indent: -.25in'>
			<span >●<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
			</span></span><span >Trishul Kumbhak: no hold after exhalation</span>
		</p>

		<p class=MsoNormal style='margin-left: 1.0in; text-indent: -.25in'>
			<span >●<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
			</span></span><span >Kechri Flutter: in anal lock</span>
		</p>

		<p class=MsoNormal style='margin-left: 1.0in; text-indent: -.25in'>
			<span >●<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
			</span></span><span >Kram Bhastrika: in anal lock</span>
		</p>

		<p class=MsoNormal>
			<span >      3. Evening Practice: </span>
		</p>

		<p class=MsoNormal style='margin-left: 1.0in; text-indent: -.25in'>
			<span >●<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
			</span></span><span >Adhar Kaali Kumbhak: 4-4-16-8</span>
		</p>

		<p class=MsoNormal style='margin-left: 1.0in; text-indent: -.25in'>
			<span >●<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
			</span></span><span >Naag Kumbhak:</span>
		</p>

		<p class=MsoNormal style='margin-left: 1.0in; text-indent: -.25in'>
			<span >●<span style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
			</span></span><span >Trinetra Kumbhak: </span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

		<p class=MsoNormal>
			<span >&nbsp;</span>
		</p>

	</div>

</body>

</html>
